$(document).ready(function () {
    
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    //var btn = document.getElementById("myBtn");


    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    $("#myBtn").click(function () {
        modal.style.display = "block";
        $("body").css("overflow", "hidden");
        $(".Sharelink").val(window.location.href);
        $(".tkw-share-link").attr('href',window.location.href);
    });

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            $("body").css("overflow", "scroll");
        }
    }
    
    $(".tkw-copy-share-url-track").click(function(){
        let trackId = $(this).attr("data-trackId");
        modal.style.display = "block";
        $("body").css("overflow", "hidden");
        $(".Sharelink").val(window.location.href+"?trackId="+trackId);
        $(".tkw-share-link").attr('href',window.location.href+"?trackId="+trackId);
    });
    
    
});



