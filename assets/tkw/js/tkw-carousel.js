$(document).ready(function () {

    $('.carousel[data-type="multi"] .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 8; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
    
    $(".tkw-carousel-next").click(function(){
        $('.carousel').carousel('next');
    });
    
    $(".tkw-carousel-previous").click(function(){
        $('.carousel').carousel('prev');
    });

    $("#genre").click(function () {
       // event.stopPropagation();
        if ($("#myCarousel").css("height") == "0px")
        {
            $("#myCarousel").css("height", "170px");
        } else
        {
            $("#myCarousel").css("height", "0px");
        }
    });

//    $("#myCarousel").on("click", function (event) {
//        event.stopPropagation();
//    });


    $("#myCarousel").css("height", "0px");
});