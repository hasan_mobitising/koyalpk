$(document).ready(function() {
    window.onpopstate = function(event) {
        location.reload()
    };
    $(document).on("click", ".tkw-body-content", function() {
        var url = $(this).attr('id');
        var ajax_url = $(this).find(".tkw-url-redirect").attr('id');
        $.ajax({
            async: !0,
            url: url,
            method: "POST",
            data: {},
            success: function(response) {
                $(".tkw-body").html(response);
                $('html, body').animate({
                    scrollTop: 0
                }, 'fast');
                $('.button-collapse').sideNav('destroy');
                $(".button-collapse").sideNav();
                var state = {
                    "canBeAnything": !0
                };
                window.history.pushState(state, "Title", ajax_url);
                expect(history.state).toEqual(state);
            }
        })
    })
})