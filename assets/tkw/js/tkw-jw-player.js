var globalPlaylist = [];

function remove_duplicates(objectsArray) {
    var usedObjects = {};

    for (var i = objectsArray.length - 1; i >= 0; i--) {
        var so = JSON.stringify(objectsArray[i]);

        if (usedObjects[so]) {
            objectsArray.splice(i, 1);

        } else {
            usedObjects[so] = true;
        }
    }

    return objectsArray;
}

function tkw_track_stopped(trackId, duration, endTime, genre, language, track, artist, album, artistId, albumId) {

    $.ajax({
        async: !0,
        url: base_url + "index.php/main/duration/",
        method: "POST",
        data: {
            trackId: trackId,
            duration: duration,
            endTime: endTime,
            genre: genre,
            language: language,
            track: track,
            artist: artist,
            album: album,
            artistId: artistId,
            albumId: albumId

        },
        success: function (response) {

        }
    })
}

function tkw_track_played(trackId, genre, language, track, artist, album, artistId, albumId) {

    let data = {
        trackId: trackId,
        genre: genre,
        language: language,
        track: track,
        artist: artist,
        album: album,
        artistId: artistId,
        albumId: albumId
    };
    $.ajax({
        async: !0,
        url: base_url + "index.php/main/played_track/",
        method: "POST",
        data: data,
        success: function (response) {

        }
    })
}

function last_track_hit(globalPlaylist) {
    let index = jwplayer('s3bubble-jwplayer').getPlaylistIndex();
    let item = jwplayer('s3bubble-jwplayer').getPlaylistItem(index);
    let title = item['title'];
    let endTime = jwplayer('s3bubble-jwplayer').getPosition();
    let duration = jwplayer('s3bubble-jwplayer').getDuration();
    for (let i = 0; i < globalPlaylist.length; i++) {
        if (globalPlaylist[i].Track == title) {
            tkw_track_stopped(globalPlaylist[i].TrackId, duration, endTime, globalPlaylist[i].Genre, globalPlaylist[i].Language, globalPlaylist[i].Track, globalPlaylist[i].Artist, globalPlaylist[i].Album, globalPlaylist[i].ArtistId, globalPlaylist[i].AlbumId);
            break;
        }
    }
}

