$(document).ready(function() {
    var limit = 4;
    var offset = 6;
    var albumId = $(".tkw-album").attr("id");
    var currentTrackPlaying = 0;
    var targetPrev = $(".track-name").find(">:first-child").attr("id");
    $(document).on("click", ".track-name", function() {
        var targetTrack = $(this).find(">:first-child");
        var targetGeneral = $(".track-name").find(">:first-child");
        var trackId = targetTrack.attr("id");
        currentTrackPlaying = trackId;
        $(".track").css("border-bottom", "1px solid #d6d6d6");
        $(this).parent().parent().parent().css("border-bottom", "2px solid #755eb6");
        if (targetGeneral.hasClass("fa-pause")) {
            console.log("eeeee");
            console.log($(".fa-pause").attr("id"));
            targetGeneral.removeClass("fa-pause");
            jwplayer('s3bubble-jwplayer').pause(!0)
        } else {
            targetTrack.addClass("fa-pause");
            if (jwplayer('s3bubble-jwplayer').getState() == "paused") {
                if (targetTrack.attr("id") == targetPrev) {
                    jwplayer('s3bubble-jwplayer').pause(!1);
                    console.log("jjjj")
                } else {
                    targetPrev = targetTrack.attr("id");
                    var filePath = $(this).parent().attr("id");
                    tkw_audio_play(filePath);
                    $(".page-footer").css("display", "none");
                    console.log("kkk")
                }
            } else {
                targetPrev = targetTrack.attr("id");
                var filePath = $(this).parent().attr("id");
                tkw_audio_play(filePath);
                $(".page-footer").css("display", "none")
            }
        }
        jwplayer().on('pause', function(e) {
            targetTrack.removeClass("fa-pause");
            var endTime = jwplayer().getPosition();
            var duration = jwplayer().getDuration();
            tkw_track_stopped(targetPrev, duration, endTime)
        });
        jwplayer().on('play', function(e) {
            targetGeneral.removeClass("fa-pause");
            targetTrack.addClass("fa-pause");
            tkw_track_played(currentTrackPlaying)
        })
    });
    $(".tkw-show-more-tracks").click(function() {
        var action = $(this).attr("id");
        $.ajax({
            async: !0,
            url: base_url + "index.php/web/more_tracks/",
            method: "POST",
            data: {
                albumId: albumId,
                action: action,
                limit: limit,
                offset: offset
            },
            success: function(response) {
                offset = offset + 4;
                if (response == "") {
                    $(".tkw-show-more-tracks").hide()
                }
                $(".tracks-list").append(response)
            }
        })
    })
});

function tkw_track_stopped(trackId, duration, endTime) {
    $.ajax({
        async: !0,
        url: base_url + "index.php/main/duration/",
        method: "POST",
        data: {
            trackId: trackId,
            duration: duration,
            endTime: endTime
        },
        success: function(response) {}
    })
}

function tkw_track_played(trackId) {
    $.ajax({
        async: !0,
        url: base_url + "index.php/main/played_track/",
        method: "POST",
        data: {
            trackId: trackId
        },
        success: function(response) {}
    })
}

function tkw_audio_play(filePath) {
    jwplayer('s3bubble-jwplayer').setup({
        "playlist": {
            "image": $(".tkw-thumbnail-image").attr("src"),
            "sources": {
                "file": filePath,
                "preload": "none",
            },
        },
        "autostart": !0,
        "width": "100%",
        "aspectratio": "16:9",
        "primary": "flash",
        "skin": {
            "name": "bekle",
            "active": "",
            "inactive": "",
            "background": ""
        },
    });
    jwplayer().onReady(function(event) {
        jwplayer().setMute(!1)
    })
}