<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

$route['sort'] = 'main/sort';
$route['category/(:any)'] = 'main/category/$1';
#$route['collection/(:any)'] = 'slug/collection/$1';
$route['artist/(:any)'] = 'slug/artist/$1';
$route['urdu'] = 'main/urdu/';
$route['punjabi'] = 'main/profile/';
$route['punjabi'] = 'main/punjabi/';
$route['saraiki'] = 'main/saraiki/';
$route['sindhi'] = 'main/sindhi/';
$route['pashto'] = 'main/pashto/';
$route['balochi'] = 'main/balochi/';
$route['hindko'] = 'main/hindko/';
$route['new/(:any)'] = 'slug/new_collect/$1';
$route['album/(:any)'] = 'slug/album/$1';
$route['album/(:any)/(:any)'] = 'slug/album/$1/$2';
#$route['trend/(:any)'] = 'slug/trend/$1';
$route['share'] = 'main/share/';
$route['general/(:any)'] = 'main/general/$1';
$route['slider/(:any)'] = 'main/slider/$1';
$route['album2/(:any)'] = 'main/album2/$1';
$route['trend/(:any)'] = 'main/trend/$1';
$route['new_collect/(:any)'] = 'main/new_collect/$1';
$route['collection/(:any)'] = 'main/collection/$1';
$route['artist/(:any)'] = 'main/artist/$1';
$route['default_controller'] = 'web';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
