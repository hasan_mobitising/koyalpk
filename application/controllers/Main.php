<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends TKW_Controller {

    protected $remoteBaseUrl;

    public function __construct() {
        parent::__construct();
        $this->initialize();
    }

    public function initialize() {

        error_reporting(0);
        $this->remoteBaseUrl = "http://13.127.28.23/musicapp/";
        if (!isset($_SESSION['lang'])) {
            $_SESSION['lang'] = "1,2,3,4,5,6,7,8,9,10,11,12,13";
        }
    }

    public function index() {
        $lang = "1,2,3,4,5,6,7,8,9,10,11,12,13";
        if (!isset($_SESSION['lang'])) {
            $_SESSION['lang'] = "1,2,3,4,5,6,7,8,9,10,11,12,13";
        }
        $lang = $_SESSION['lang'];
        $languages = json_decode(tkw_remote_access($this->remoteBaseUrl . "/?request=gen-lang-list"), true)['Response']['Languages'];
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $lang . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function languages($lang) {
        $_SESSION['lang'] = $lang;
        $languages = json_decode(tkw_remote_access($this->remoteBaseUrl . "/?request=gen-lang-list"), true)['Response']['Languages'];
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        echo $this->load->view("components/home-body-content", $data);
    }

    public function bhangra() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Bhangra";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function classic() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Classic";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function folk() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Folk";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function ghazal() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Ghazal";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function patriotic() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Patriotic";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function romantic() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Romantic";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function sufi() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Sufi";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function pop() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Pop";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function rap() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Rap";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function comedy() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Comedy";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function dhamal() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Dhamal";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function funny() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Funny";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function instrumental() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Instrumental";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function joke() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Joke";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function mushaira() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Mushaira";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function poetry() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Poetry";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function political() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Political";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function qawali() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Qawali";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function wedding() {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-sd&action=Wedding";
        $response = json_decode(tkw_remote_access($url), true);
        echo $this->load->view("tracks", $response['Response']);
    }

    public function urdu() {
        $_SESSION['lang'] = 1;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function punjabi() {
        $_SESSION['lang'] = 2;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function saraiki() {
        $_SESSION['lang'] = 3;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function sindhi() {
        $_SESSION['lang'] = 4;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function pashto() {
        $_SESSION['lang'] = 5;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function balochi() {
        $_SESSION['lang'] = 6;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function hindko() {
        $_SESSION['lang'] = 7;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function home_page_content($data = null) {
        if (!empty($data)) {
            $data['Languages'] = $languages;
            foreach ($data['HomePageContent']['Slider'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/slider/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/slider/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['New'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/new_collect/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/new_collect/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Collections'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/collection/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/collection/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Popular'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/trend/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/trend/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Artists'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/profile/' . $value['Id'];
                $value['URLRedirect'] = base_url() . '/main/profile/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
            }
            foreach ($data['HomePageContent']['Albums'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/album/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/album/' . $value['Id'];
            }
        }

        return $data;
    }

    public function album($albumId) {
        //$url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=album&Id=" . $albumId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=0&offset=0";
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=album&Id=" . $albumId . "&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $this->load->view("tracks", $data);
    }

    public function album2($albumId) {
        //$url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=album&Id=" . $albumId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=0&offset=0";
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=general&Id=" . $albumId . "&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $this->load->view("tracks", $data);
    }

    public function general($albumId) {
        //$url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=album&Id=" . $albumId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=0&offset=0";
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=general&Id=" . $albumId . "&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $this->load->view("tracks", $data);
    }

    public function slider($albumId) {
        //$url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=album&Id=" . $albumId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=0&offset=0";
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=slider&Id=" . $albumId . "&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $this->load->view("tracks", $data);
    }

    public function more_tracks() {

        $albumId = $this->input->post('albumId');
        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=" . $action . "&Id=" . $albumId . "&orderBy=Artist&languageId=" . $_SESSION['lang'] . "&userType=guest&orderAs=ASC&&limit=" . $limit . "&offset=" . $offset;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            echo $this->load->view("components/append-tracks", $data, TRUE);
        } else {
            echo "no result";
        }
        exit;
    }

    public function collection($collectionId) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=collect&Id=" . $collectionId . "&orderBy=Artist&languageId=" . $_SESSION['lang'] . "&userType=guest&orderAs=ASC&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "collect";
        $this->load->view("tracks", $data);
    }

    public function artist($artistId) {
        $url = $this->remoteBaseUrl . "?request=get-albums&action=artist&id=" . $artistId . "&limit=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (empty($data['Albums'])) {
            $data["Title"] = "No Record Found";
        } else {
            $data["Title"] = $data['Albums'][0]['Artist'];
        }
        $data["Directory"] = "general";
        $this->load->view("more", $data);
    }

    public function new_collect($collectionId) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=new&Id=" . $collectionId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&orderBy=Artist&orderAs=ASC&limit=0&offset=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "new";
        $this->load->view("tracks", $data);
    }

    public function trend($collectionId) {
//        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=trend&Id=" . $collectionId . "&languageId=" . $_SESSION['lang'] . "&userType=guest&orderBy=Artist&orderAs=ASC&limit=0&offset=0";
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=trend&Id=" . $collectionId . "&userId=0";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "trend";
        $this->load->view("tracks", $data);
    }

    public function category($categoryId) {
        $data = tkw_get_category_content($categoryId, $this->remoteBaseUrl);
        $this->load->view("more", $data);
    }

    public function category_ajax($categoryId) {
        $_SESSION['lang'] = $this->input->post('languageId');
        $data = tkw_get_category_content($categoryId, $this->remoteBaseUrl);
        echo $this->load->view("components/albums", $data);
    }

    public function more_albums() {

        $action = $this->input->post('action');
        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');

        switch ($action):
            case "new":
                $url = $this->remoteBaseUrl . "/?request=get-albums&orderAs=DESC&orderBy=ReleaseDate&action=general&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "new";
                $data["Title"] = "New Collections";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
            case "trend":
                //$url = $this->remoteBaseUrl . "/?request=get-albums&action=trend&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=popular&languageId=" . $_SESSION['lang'] . "&userType=guest&orderBy=Views&orderAs=DESC&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "trend";
                $data["Title"] = "Popular";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
            case "collect":
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=collect&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "collect";
                $data["Title"] = "Collections";
                $data["Directory"] = "collection";
                $data['Count'] = count($data['Albums']);
                break;
            case "artist":
                $url = $this->remoteBaseUrl . "/?request=get-artists2&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "artist";
                $data["Title"] = "Artists";
                $data["Directory"] = "artist";
                $data['Count'] = count($data['Artists']);
                break;
            default:
                $url = $this->remoteBaseUrl . "/?request=get-albums&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "albums";
                $data["Title"] = "Albums";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
        endswitch;
        echo $this->load->view("components/catlog", $data);
    }

    public function search($keyword = null) {
        if ($this->input->post('txtKeyword')) {
            $keyword = urlencode($this->input->post('txtKeyword'));
            redirect(base_url() . 'main/search/' . $keyword . "?keyword=" . $keyword);
        }
        $url = $this->remoteBaseUrl . "?request=search1&limit=5&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0&keyword=" . $keyword;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            foreach ($data['SearchResult']['Tracks'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/general/' . $value['AlbumId'] . '?trackId=' . $value['TrackId'];
                $value['URLRedirect'] = base_url() . 'main/general/' . $value['AlbumId'] . '/' . str_replace(" ", "-", $value['Name']) . '?trackId=' . $value['TrackId'];
            }
            foreach ($data['SearchResult']['Albums'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/general/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/general/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
            }
            foreach ($data['SearchResult']['Artists'] as $key => &$value) {
                $value['Link'] = base_url() . 'web/artist/' . $value['Id'];
                $value['URLRedirect'] = base_url() . 'main/artist/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
            }
        }
        $this->load->view("search", $data);
    }

    public function search_more() {
        $keyword = urlencode($this->input->post('keyword'));
        $url = $this->remoteBaseUrl . "?request=search1&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $this->input->post('limit') . "&offset=" . $this->input->post('offset') . "&keyword=" . $keyword;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            switch ($this->input->post('type')):
                case "Tracks":
                    foreach ($data['SearchResult']['Tracks'] as $key => &$value) {
                        $value['Link'] = base_url() . 'web/general/' . $value['AlbumId'] . '?trackId=' . $value['TrackId'];
                        $value['URLRedirect'] = base_url() . 'main/general/' . $value['AlbumId'] . '/' . str_replace(" ", "-", $value['Name']) . '?trackId=' . $value['TrackId'];
                    }
                    $data['Data'] = $data['SearchResult']['Tracks'];
                    break;
                case "Albums":
                    foreach ($data['SearchResult']['Albums'] as $key => &$value) {
                        $value['Link'] = base_url() . 'web/general/' . $value['Id'];
                        $value['URLRedirect'] = base_url() . 'main/general/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
                    }
                    $data['Data'] = $data['SearchResult']['Albums'];
                    break;
                case "Artists":
                    foreach ($data['SearchResult']['Artists'] as $key => &$value) {
                        $value['Link'] = base_url() . 'web/artist/' . $value['Id'];
                        $value['URLRedirect'] = base_url() . 'main/artist/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
                    }
                    $data['Data'] = $data['SearchResult']['Artists'];
                    break;
            endswitch;
            echo $this->load->view("components/append-search", $data, TRUE);
        } else {
            echo json_encode("no result");
        }
        exit;
    }

    public function share() {
        $albumId = $this->input->get('albumId');
        $action = $this->input->get('action');
        $trackId = $this->input->get('trackId');
        redirect(base_url() . "index.php/main/" . $action . "/" . $albumId . "?trackId=" . $trackId);
        //redirect(base_url()."index.php/".$action."/".$albumId."/".$trackId);
    }

    public function played_track() {
        $trackId = urlencode(str_replace("track-", "", $this->input->post('trackId')));
        $language = $this->input->post('language');
        $genre = $this->input->post('genre');
        $track = $this->input->post('track');
        $artist = $this->input->post('artist');
        $album = $this->input->post('album');
        $artistId = $this->input->post('artistId');
        $albumId = $this->input->post('albumId');
        $url = $this->remoteBaseUrl . "/?request=played&trackId=" . urlencode($trackId) . "&Language=" . urlencode($language) . "&Genre=" . urlencode($genre) . "&Track=" . urlencode($track) . "&Artist=" . urlencode($artist) . "&Album=" . urlencode($album) . "&ArtistId=" . urlencode($artistId) . "&AlbumId=" . urlencode($albumId);
        $response = json_decode(tkw_remote_access($url), true);
    }

    public function duration() {
        $ipAddress = $this->input->ip_address();
        $trackId = str_replace("track-", "", $this->input->post('trackId'));
        $duration = $this->input->post('duration');
        $endTime = $this->input->post('endTime');
        $language = $this->input->post('language');
        $genre = $this->input->post('genre');
        $track = $this->input->post('track');
        $artist = $this->input->post('artist');
        $album = $this->input->post('album');
        $artistId = $this->input->post('artistId');
        $albumId = $this->input->post('albumId');
        $url = $this->remoteBaseUrl . "/?request=duration&Ip=" . urlencode($ipAddress) . "&TrackId=" . urlencode($trackId) . "&Duration=" . urlencode($duration) . "&EndTime=" . urlencode($endTime) . "&Language=" . urlencode($language) . "&Genre=" . urlencode($genre) . "&Track=" . urlencode($track) . "&Artist=" . urlencode($artist) . "&Album=" . urlencode($album) . "&ArtistId=" . urlencode($artistId) . "&AlbumId=" . urlencode($albumId);
        $response = json_decode(tkw_remote_access($url), true);
    }

    public function profile($id) {
        $url = $this->remoteBaseUrl . "?request=get-artist-profile&id=" . $id;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        foreach ($data['Albums'] as $key => &$value) {
            $value['Link'] = base_url() . 'web/general/' . $value['Id'];
            $value['URLRedirect'] = base_url() . 'main/general/' . $value['Id'] . '/' . str_replace(" ", "-", $value['Name']);
        }
        foreach ($data['Tracks'] as $key => &$val) {
            $val['Link'] = base_url() . "web/top_tracks/" . $id . "/" . str_replace(' ', '-', $val['Name'])."?trackId=".$val['Id'];
            $val['URLRedirect'] = base_url() . "main/top_tracks/" . $id . "/" . str_replace(' ', '-', $val['Name'])."?trackId=".$val['Id'];
        }
        $data['AllAlbumsLinkMain'] = base_url() . 'index.php/main/artist/' . $id . '/' . str_replace('-', ' ', $data['Artist']);
        $data['AllAlbumsLinkWeb'] = base_url() . 'index.php/web/artist/' . $id . '/' . str_replace('-', ' ', $data['Artist']);
        //tkw_array_debug($data);
        $this->load->view("profile", $data);
    }

    public function top_tracks($id) {
        $url = $this->remoteBaseUrl . "?request=get-artist-profile&id=" . $id;
        $response = json_decode(tkw_remote_access($url), true);
        $data['Tracks'] = $response['Response']['Tracks'];
        foreach($data['Tracks'] as $key => &$val){
            $val['TrackId']=$val['Id'];
        }
        $this->load->view('tracks', $data);
        //tkw_array_debug($data['Tracks']);
    }

}
