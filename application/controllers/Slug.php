<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slug extends TKW_Controller {

    protected $remoteBaseUrl;

    public function __construct() {
        parent::__construct();
        $this->initialize();
    }

    public function initialize() {

        error_reporting(0);
        $this->remoteBaseUrl = "http://13.127.28.23/musicapp/";
    }

    public function index() {
        $lang = "1,2,3,4,5,6,7,8,9,10,11,12,13";
        if (isset($_SESSION['lang'])) {
            $lang = $_SESSION['lang'];
        }
        $languages = json_decode(tkw_remote_access($this->remoteBaseUrl . "/?request=gen-lang-list"), true)['Response']['Languages'];
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $lang . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        $this->load->view("home", $data);
    }

    public function languages($lang) {
        $_SESSION['lang'] = $lang;
        $url = $this->remoteBaseUrl . "/?request=set-lang&userId=0&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=6";
        $response = json_decode(tkw_remote_access($url), true);
        $data = $this->home_page_content($response['Response']);
        echo $this->load->view("components/home-body-content", $data);
    }

    public function home_page_content($data = null) {
        if (!empty($data)) {
            $data['Languages'] = $languages;
            foreach ($data['HomePageContent']['Slider'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/album/' . $value['SliderAlbumId'];
            }
            foreach ($data['HomePageContent']['New'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/new_collect/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Collections'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/collection/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Popular'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/trend/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Artists'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/artist/' . $value['Id'];
            }
            foreach ($data['HomePageContent']['Albums'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/album/' . $value['Id'];
            }
        }

        return $data;
    }

    //Done
    public function album($albumId) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=album&Id=" . $albumId;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $this->load->view("tracks", $data);
    }

    public function more_tracks() {

        $albumId = $this->input->post('albumId');
        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=" . $action . "&Id=" . $albumId . "&orderBy=Artist&languageId=" . $_SESSION['lang'] . "&userType=guest&orderAs=ASC&&limit=" . $limit . "&offset=" . $offset;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            echo $this->load->view("components/append-tracks", $data, TRUE);
        } else {
            echo "no result";
        }
        exit;
    }

    //Done
    public function collection($collection) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=collect&album=" . $collection;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "collect";
        $this->load->view("tracks", $data);
    }

    //Done
    public function artist($artist) {
        $url = $this->remoteBaseUrl . "?request=get-albums&action=artist&artist=" . str_replace("%20", "_", $artist);
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (empty($data['Albums'])) {
            $data["Title"] = "No Record Found";
        } else {
            $data["Title"] = $data['Albums'][0]['Artist'];
        }
        $data["Directory"] = "album";
        $this->load->view("more", $data);
    }

    //Done
    public function new_collect($new_collect) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks-web&action=new&album=" . str_replace("%20", "_", $new_collect);
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "new";
        $this->load->view("tracks", $data);
    }

    //Done
    public function trend($trend) {
        $url = $this->remoteBaseUrl . "/?request=get-tracks&action=trend&album=" . str_replace("%20", "_", $trend);
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        $data['Action'] = "trend";
        $this->load->view("tracks", $data);
    }

    public function category($categoryId) {
        $data = $this->get_category_content($categoryId);
        $this->load->view("more", $data);
    }

    public function category_ajax($categoryId) {
        $_SESSION['lang'] = $this->input->post('languageId');
        $data = $this->get_category_content($categoryId);
        echo $this->load->view("components/albums", $data);
    }

    public function get_category_content($categoryId) {

        switch ($categoryId):
            case 1:
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=new&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "new";
                $data["Title"] = "New";
                $data["Directory"] = "new_collect";
                $data['Count'] = count($data['Albums']);
                break;
            case 2:
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=trend&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "trend";
                $data["Title"] = "Popular";
                $data["Directory"] = "trend";
                $data['Count'] = count($data['Albums']);
                break;
            case 3:
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=collect&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "collect";
                $data["Title"] = "Collections";
                $data["Directory"] = "collection";
                $data['Count'] = count($data['Albums']);
                break;
            case 4:
                $url = $this->remoteBaseUrl . "/?request=get-artists1&limit=24&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "artist";
                $data["Title"] = "Artists";
                $data["Directory"] = "artist";
                $data['Count'] = count($data['Artists']);
                break;
            default:
                $url = $this->remoteBaseUrl . "/?request=get-albums&limit=24&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "albums";
                $data["Title"] = "Albums";
                $data["Directory"] = "album";
                $data['Count'] = count($data['Albums']);
                break;
        endswitch;

        return $data;
    }

    public function more_albums() {

        $action = $this->input->post('action');
        $limit = $this->input->post('limit');
        $offset = $this->input->post('offset');

        switch ($action):
            case "new":
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=new&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "new";
                $data["Title"] = "New Collections";
                $data["Directory"] = "album";
                $data['Count'] = count($data['Albums']);
                break;
            case "trend":
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=trend&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "trend";
                $data["Title"] = "Popular";
                $data["Directory"] = "album";
                $data['Count'] = count($data['Albums']);
                break;
            case "collect":
                $url = $this->remoteBaseUrl . "/?request=get-albums&action=collect&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "collect";
                $data["Title"] = "Collections";
                $data["Directory"] = "album";
                $data['Count'] = count($data['Albums']);
                break;
            case "artist":
                $url = $this->remoteBaseUrl . "/?request=get-artists1&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "artist";
                $data["Title"] = "Artists";
                $data["Directory"] = "artist";
                $data['Count'] = count($data['Artists']);
                break;
            default:
                $url = $this->remoteBaseUrl . "/?request=get-albums&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $limit . "&offset=" . $offset;
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "albums";
                $data["Title"] = "Albums";
                $data["Directory"] = "album";
                $data['Count'] = count($data['Albums']);
                break;
        endswitch;
        echo $this->load->view("components/catlog", $data);
    }

    public function search($keyword = null) {
        if ($this->input->post('txtKeyword')) {
            $keyword = $this->input->post('txtKeyword');
            redirect('main/search/' . $keyword . "?keyword=" . $keyword);
        }
        $url = $this->remoteBaseUrl . "?request=search&limit=5&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0&keyword=" . $keyword;
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            foreach ($data['SearchResult']['Tracks'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/album/' . $value['AlbumId'] . '#' . $value['TrackId'];
            }
            foreach ($data['SearchResult']['Albums'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/album/' . $value['Id'];
            }
            foreach ($data['SearchResult']['Artists'] as $key => &$value) {
                $value['Link'] = base_url() . 'index.php/artist/' . $value['Id'];
            }
        }
        $this->load->view("search", $data);
    }

    public function search_more() {

        $url = $this->remoteBaseUrl . "?request=search&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=" . $this->input->post('limit') . "&offset=" . $this->input->post('offset') . "&keyword=" . $this->input->post('keyword');
        $response = json_decode(tkw_remote_access($url), true);
        $data = $response['Response'];
        if (!empty($data)) {
            switch ($this->input->post('type')):
                case "Tracks":
                    foreach ($data['SearchResult']['Tracks'] as $key => &$value) {
                        $value['Link'] = base_url() . 'index.php/album/' . $value['AlbumId'] . '#' . $value['TrackId'];
                    }
                    $data['Data'] = $data['SearchResult']['Tracks'];
                    break;
                case "Albums":
                    foreach ($data['SearchResult']['Albums'] as $key => &$value) {
                        $value['Link'] = base_url() . 'index.php/album/' . $value['Id'];
                    }
                    $data['Data'] = $data['SearchResult']['Albums'];
                    break;
                case "Artists":
                    foreach ($data['SearchResult']['Artists'] as $key => &$value) {
                        $value['Link'] = base_url() . 'index.php/artist/' . $value['Id'];
                    }
                    $data['Data'] = $data['SearchResult']['Artists'];
                    break;
            endswitch;
            echo $this->load->view("components/append-search", $data, TRUE);
        } else {
            echo json_encode("no result");
        }
        exit;
    }

    public function share() {
        $albumId = $this->input->get('albumId');
        $action = $this->input->get('action');
        $trackId = $this->input->get('trackId');
        redirect(base_url() . "index.php/main/" . $action . "/" . $albumId . "?trackId=" . $trackId);
        //redirect(base_url()."index.php/".$action."/".$albumId."/".$trackId);
    }

}
