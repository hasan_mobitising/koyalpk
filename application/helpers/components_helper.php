<?php

function tkw_base_url() {
    return "http://" . $_SERVER['SERVER_NAME'];
}

function tkw_slider($images) {

    echo '<div class = "slider">';
    echo '<ul class = "slides">';
    foreach ($images as $key):
        echo '<li>';
        echo '<img src="' . $key . '">';
        echo '<div class = "caption center-align">';
        echo '<h3></h3>';
        echo '<h5 class = "light grey-text text-lighten-3"></h5>';
        echo '</div>';
        echo '</li>';
    endforeach;

    echo '</ul>';
    echo '</div>';
}

function tkw_catlog($section, $data) {
    echo '<div class="col s12">';
    echo '<h5 class="k-title">' . $section . '</h5>';
    foreach ($data as $key):
        echo '<div class="col s2 albums-catlog">';
        echo '<a href="#">';
        echo '<div class="card small albums-img">';
        echo '<div class="card-image album-image">';
        echo '<img src="' . $key['ThumbnailImage'] . '">';
        echo '</div>';
        echo '<div class="card-content ablum-name">';
        echo '<h6>' . $key['Name'] . '</h6>';
        echo '</div>';
        echo '</div>';
        echo '</a>';
        echo '</div>';
    endforeach;
    echo '<span class="view-all-title"><a href="' . base_url() . 'index.php/albums"><i class="material-icons left icon-spacing">add_circle</i>More</a></span>';
    echo '</div>';
}

function tkw_array_debug($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
    die;
}

function tkw_album_catlog() {
    echo '<div class="col s2 albums-catlog catlog-space-top">';
    echo '<a href="album_play.html">';
    echo '<div class="card small albums-img">';
    echo '<div class="card-image album-image">';
    echo '<img src="' . base_url() . 'assets/Img/album-img-1.jpg">';
    echo '<span class="card-title"> <i class="fa fa-play album-play" aria-hidden="true"></i></span>';
    echo '</div>';
    echo '</div>';
    echo '</a>';
    echo '</div>';
}

function global_playlist($Tracks) {

    foreach ($Tracks as $track) {
        $globalPlaylist[] = array(
            'TrackId' => $track['TrackId'],
            'Genre' => $track['Genre'],
            'Language' => $track['Language'],
            'Track' => $track['Name'],
            'Artist' => $track['Artist'],
            'Album' => $track['Album'],
            'ArtistId' => $track['ArtistId'],
            'AlbumId' => $track['AlbumId'],
            'Image' => $track['ThumbnailImage']
        );
    }
    return json_encode($globalPlaylist);
}

function get_playlist($Tracks) {
    if (isset($Tracks)) {
        foreach ($Tracks as $track) {
            $playlist[] = array(
                'title' => $track['Name'],
                'image' => $track['ThumbnailImage'],
                'file' => $track['TrackUrl']
            );
        }
        return json_encode($playlist);
    }
}

function tkw_genre_content() {
    $url = "http://13.127.28.23/musicapp/?request=search-content";
    $response = json_decode(tkw_remote_access($url), true);

    foreach ($response['Response'] as $key => &$value) {
        $value['Link'] = base_url() . 'web/genre_tracks/' . $value['Name'] . '/';
        $value['URLRedirect'] = base_url() . 'main/' . strtolower($value['Name']) . '/';
    }
    return $response['Response'];
}

function tkw_get_category_content($categoryId, $remoteBaseUrl) {
        switch ($categoryId):
            case 1:
                $url = $remoteBaseUrl . "/?request=get-albums&action=general&orderAs=DESC&orderBy=ReleaseDate&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "new";
                $data["Title"] = "New";
                $data["TitleInUrdu"] = "نئے گانے";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
            case 2:
                $url = $remoteBaseUrl . "/?request=get-albums&action=popular&languageId=" . $_SESSION['lang'] . "&userType=guest&orderBy=Views&orderAs=DESC&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "trend";
                $data["Title"] = "Popular";
                $data["TitleInUrdu"] = "مشہور";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
            case 3:
                $url = $remoteBaseUrl . "/?request=get-albums&action=collect&languageId=" . $_SESSION['lang'] . "&userType=guest&limit=24&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "collect";
                $data["Title"] = "Collections";
                $data["TitleInUrdu"] = "کلیکشن";
                $data["Directory"] = "collection";
                $data['Count'] = count($data['Albums']);
                break;
            case 4:
                //$url = $this->remoteBaseUrl . "/?request=get-artists1&limit=24&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0";
                $url = $remoteBaseUrl . "/?request=get-artists2&limit=24";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "artist";
                $data["Title"] = "Artists";
                $data["TitleInUrdu"] = "گلوکار";
                $data["Directory"] = "profile";
                $data['Count'] = count($data['Artists']);
                break;
            default:
                $url = $remoteBaseUrl . "/?request=get-albums&limit=24&languageId=" . $_SESSION['lang'] . "&userType=guest&offset=0";
                $response = json_decode(tkw_remote_access($url), true);
                $data = $response['Response'];
                $data["Action"] = "albums";
                $data["Title"] = "Albums";
                $data["TitleInUrdu"] = "البمز";
                $data["Directory"] = "general";
                $data['Count'] = count($data['Albums']);
                break;
        endswitch;
        return $data;
    }
