<?php

function tkw_remote_access($url) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 431d097c-c6cc-81e3-0a2d-a84190ffffdc"
        ),
    ));
    $response = curl_exec($curl);
    //$err = curl_error($curl);

    curl_close($curl);
    return $response;
}
