<div id="myModal" class="smodal">
    <div class="smodal-content">
        <div class="smodal-body">
            <h5>Share</h5>
            <span class="close">&times;</span>
            <div class="col s12 socialshares">
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/fb-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/tw-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/wtp-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/lnk-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/gp-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/sms-icon.png" onerror="imgError(this);"/></a>
                <a class="tkw-share-link" href="javascript:void(0)"><img src="<?php echo base_url() ?>images/email-icon.png" onerror="imgError(this);"/></a>
            </div>
            <div class="col s12">
                <input type="text" name="ShareURL" class="Sharelink" id="tkw-share-url" value=''/>
                <button class="waves-effect waves-light btn copy-btn tkw-copy-share-url" data-clipboard-target="#tkw-share-url"><i class="material-icons left">content_copy</i>Copy</button>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js"></script>
<Script>
    $(document).ready(function () {
        new Clipboard('.tkw-copy-share-url');
    });
</script>
