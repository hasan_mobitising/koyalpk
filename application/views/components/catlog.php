<?php
$content = isset($Albums) ? $Albums : $Artists;
foreach ($content as $key):
    ?>
    <div class="col s2 albums-catlog catlog-space-top">
        <a class="tkw-body-content" href="javascript:void(0)" id="<?php echo base_url() . 'web/' . $Directory . '/' . $key['Id'] ?>" title="<?php echo $key['Name'] ?>">
            <span class="tkw-url-redirect" id="<?php echo base_url() . 'main/' . $Directory . '/' . $key['Id'] . '/' . str_replace(" ", "-", $key['Name']) ?>"></span>
            <div class="card small albums-img">
                <div class="card-image album-image">
                    <img src="<?php echo $key['ThumbnailImageWeb'] ?>" onerror="imgError(this);">
                    <span class="card-title"> <i class="fa fa-music album-play" aria-hidden="true"></i></span>
                </div>
                <div class="card-content margin-top-10">
                    <span class="ellipsis" id="singer" ><?php echo $key['Name'] ?></span>
                    <span class="album-by">
                        <span class="album-artist ellipsis"> 
                            <?php echo (isset($key['Artist'])) ? $key['Artist'] : ""; ?>
                        </span>
                    </span>
                </div>
            </div>
        </a>
    </div>
<?php endforeach; ?>
