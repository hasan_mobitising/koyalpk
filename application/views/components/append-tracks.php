<?php
if (isset($Tracks)):
    foreach ($Tracks as $key) :
        ?>
        <div class="col s12 track">
            <div class="tracklist-margin">
                <div class="col s2 albums-catlog Track-catlog">
                    <div class="card small albums-img tracks-img">
                        <div class="card-image album-image tracksmallimg tkw-album-image" data-trackTitle = "<?php echo $key['Name']; ?>" data-trackImage="<?php echo $key['ThumbnailImage']; ?>">
                            <img src="<?php echo $key['ThumbnailImage']; ?>" onerror="imgError(this);">
                            <span class="card-title"> <i class="fa fa-play album-play" id="<?php echo "track-" . $key['TrackId']; ?>" data-trackTitle = "<?php echo $key['Name']; ?>" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col s7 track-detail">
                    <a class="track-name">
                        <span class="tracks-name ellipsis tracksmallimg" data-trackTitle = "<?php echo $key['Name']; ?>"><?php echo " " . $key['Name']; ?></span></a>
                    <a class="by-artist ellipsis" href="<?php echo base_url() . 'main/profile/' . $key['ArtistId'] . '/' . str_replace(' ', '-', $key['Artist']) ?>"><?php echo $key['Artist']; ?></a>
                </div>
                <div class="col s3 padding-zero">
                    <div class="col s6 padding-zero">
                        <i class="fa fa-thumbs-up padding-zero option-icon" id="<?php echo "track-" . $key['TrackId']; ?>" aria-hidden="true"></i>
                        <span class="options-txt">Like</span>
                    </div>
                    <div class="col s6 padding-zero">
                        <i class="fa fa-share padding-zero option-icon Modal tkw-copy-share-url-track" id="<?php echo "track-" . $key['TrackId']; ?> " data-trackId="<?php echo $key['TrackId']; ?> " aria-hidden="true"></i>
                        <span class="options-txt">Modal</a></span>									
                    </div>
                </div>
            </div>
        </div>

        <?php
    endforeach;
endif;
?>