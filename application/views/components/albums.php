<div class="col s12" id="<?php echo $Title; ?>">
    <div id="titles">
        <h5 class="k-title home_title"><span><?php echo $Title; ?></span></h5>
        <h5 class="k-title home_urdutitle"><span><?php echo $TitleInUrdu; ?></span></h5>
    </div>
    <div class = "tkw-sorted-content">
        <?php $this->load->view('components/catlog'); ?>
    </div>
</div>
<?php if ($Count >= 24) : ?>
    <div class="tkw-count" id="<?php echo $Title; ?>">
        <div class="show-more"> <span class="view-all-title more-album-btn"><a href="javascript:void(0)" class="tkw-show-more"><i class="material-icons right icon-spacing">arrow_downward</i> Show More</a></span></div>
    </div>
<?php endif; ?>

<!--Note: This div is dependent for different data return on show more content -->
<div class="tkw-action" id="<?php echo (isset($Action)) ? $Action : ''; ?>"></div>
