<?php foreach ($Data as $subValue): ?>
    <div class="col m9 search-result ">
        <div class="col s2 albums-catlog catlog-space-top">
            <a href="<?php echo $subValue['Link'] ?>">
                <div class="card small albums-img">
                    <div class="card-image album-image">
                        <img src="<?php echo $Tracks[0]['ThumbnailImage']; ?>" onerror="imgError(this);">
                        <span class="card-title"> <i class="fa fa-play album-play" aria-hidden="true"></i></span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col m10 mble-search">
            <div class="col m7 search-title">
                <span class="artist">
                    <a class="tkw-body-content" id="<?php echo $subValue['Link'] ?>" href="javascript:void(0)">
                        <span class="tkw-url-redirect" id="<?php echo $subValue['URLRedirect']; ?>"></span>
                        <?php echo $subValue['Name'] ?>
                    </a>
                </span>
                <?php if (isset($subValue['Album'])) : ?>
                    <span class="album"><?php echo $subValue['Album'] ?></span>
                <?php endif; ?>
                <?php if (isset($subValue['Artist'])) : ?>
                    <span class="author"><?php echo $subValue['Artist'] ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
