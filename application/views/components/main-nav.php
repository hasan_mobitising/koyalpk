<nav style="position:fixed;z-index:9999;">
    <div class="nav-wrapper">
        <a href="javascript:void(0)" class="brand-logo logo tkw-nav-link" id="lang-1,2,3,4,5,6,7,8,9,10,11,12,13"><img src="<?php echo base_url() ?>images/logo1.png" alt="logo"/></a>
        <a href="javascript:void(0)" data-activates="mobile-demo" class=" left button-collapse"><i class="material-icons">menu</i></a>

        <!--        For Desktop-->
        <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a class="tkw-nav-link" id="lang-1" href="javascript:void(0)">Urdu</a></li>
            <li><a class="tkw-nav-link" id="lang-2" href="javascript:void(0)">Punjabi</a></li>
            <li><a class="tkw-nav-link" id="lang-3" href="javascript:void(0)">Saraiki</a></li>
            <li><a class="tkw-nav-link" id="lang-4" href="javascript:void(0)">Sindhi</a></li>
            <li><a class="tkw-nav-link" id="lang-5" href="javascript:void(0)">Pashto</a></li>
            <li><a class="tkw-nav-link" id="lang-6" href="javascript:void(0)">Balochi</a></li>
            <li><a class="tkw-nav-link" id="lang-7" href="javascript:void(0)">Hindko</a></li>
            <li id="genre"><a>Genre</a></li>
        </ul>

        <!--For Mobile-->
        <ul class="side-nav" id="mobile-demo">
            <li style="display:inline-block;"><a href="javascript:void(0)" class="brand-logo logo tkw-nav-link mble-logo" id="lang-1,2,3,4,5,6,7,8,9,10,11,12,13"><img src="<?php echo base_url() ?>images/logo1.png" alt="logo"/></a></li>
            <li><a class="tkw-nav-link" id="lang-1" href="javascript:void(0)">Urdu</a></li>
            <li><a class="tkw-nav-link" id="lang-2" href="javascript:void(0)">Punjabi</a></li>
            <li><a class="tkw-nav-link" id="lang-3" href="javascript:void(0)">Saraiki</a></li>
            <li><a class="tkw-nav-link" id="lang-4" href="javascript:void(0)">Sindhi</a></li>
            <li><a class="tkw-nav-link" id="lang-5" href="javascript:void(0)">Pashto</a></li>
            <li><a class="tkw-nav-link" id="lang-6" href="javascript:void(0)">Balochi</a></li>
            <li><a class="tkw-nav-link" id="lang-7" href="javascript:void(0)">Hindko</a></li>
        </ul>

        <?php $this->load->view("components/search-box"); ?>
   <!--      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
           <li><a href="javascript:void(0)"> <i class="material-icons left">account_circle</i> login</a></li>
            <li><a href="javascript:void(0)"><i class="material-icons left">usb</i> Sign up</a></li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
            <li><a href="javascript:void(0)">login</a></li>
            <li><a href="javascript:void(0)">Sign up</a></li>
        </ul>-->
    </div>	
    <?php $this->load->view('components/genre-box'); $this->load->view('components/share-modal');?>
</nav>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-share-modal.js') ?>"></script>
<script>
    $(document).ready(function () {

        window.onpopstate = function (event) {
            location.reload();
        };

        $(".tkw-nav-link").click(function () {
            var id = $(this).attr("id");
            var url = base_url + "web/languages/" + id.replace("lang-", "");
            var text = $(this).text();
            if (id != "lang-1,2,3,4,5,6,7,8,9,10,11,12,13") {
                var ajax_url = base_url + text.toLowerCase();
            } else {
                var ajax_url = base_url;
            }

            $.ajax({
                url: url,
                data: {},
                success: function (response) {
                    $(".tkw-body").html(response);
                    $('.button-collapse').sideNav('destroy');
                    $(".button-collapse").sideNav();
                    var state = {
                        "canBeAnything": true
                    };
                    window.history.pushState(state, "Title", ajax_url);
                    expect(history.state).toEqual(state);
                    $('html, body').animate({scrollTop: 0}, 'fast');

                }
            });
        });
    });
</script>

