<div class="genrebox2">
    <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="5000" id="myCarousel">
        <div class="carousel-inner">
            <?php foreach (tkw_genre_content() as $key): ?>
                <div class="item active">
                    <div class="genrelist">
                        <a class="tkw-body-content" href="javascript:void(0)" id="<?php echo $key['Link']; ?>" class="thumbnail">
                            <img src="<?php echo $key['ThumbnailImage']; ?>" onerror="imgError(this);">
                            <span class="tkw-url-redirect" id="<?php echo $key['URLRedirect']; ?>"><?php echo $key['Name']; ?></span>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <a class="left carousel-control tkw-carousel-previous" href="javascript:void(0)" data-slide="prev"><i class="material-icons">arrow_back</i></a>
        <a class="right carousel-control tkw-carousel-next" href="javascript:void(0)" data-slide="next"><i class="material-icons">arrow_forward</i></a>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-carousel.js') ?>"></script>