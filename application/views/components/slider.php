<div class="tkw-slider-content">
<div class = "slider">
    <ul class = "slides">
        <?php foreach ($HomePageContent['Slider'] as $key): ?>
            <li>
                <a class="tkw-body-content" id="<?php echo $key['Link']; ?>" href="javascript:void(0)" >
                    <img src="<?php echo $key['SliderImageWeb']; ?>">
                    <div class = "caption center-align">
                        <h3></h3>
                        <h5 class = "light grey-text text-lighten-3"></h5>
                    </div>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<script type="text/javascript">
    $('.slider').slider();
</script>
</div>