<div class="row">
    <div class="profile-scroll">
        <a href="Profile.html"><i class="fa fa-heart fa_profile" aria-hidden="true"></i></a>
    </div>
    <?php
    $this->load->view('components/main-nav');
    $this->load->view('components/slider');
    ?>
</div>
<?php unset($HomePageContent['Slider'], $HomePageContent['Names'] ); ?>
<div class="row center-albums home">
    <?php foreach ($HomePageContent as $key => $value): ?>

        <div class="col s12" id="<?php echo $key; ?>">
            <div id="titles">
                <h5 class="k-title home_title"><span><?php echo $key; ?></span></h5>
                <h5 class="k-title home_urdutitle"><span><?php echo $value[0]['CatInUrdu']; ?></span></h5>
            </div>
            <?php foreach ($value as $subValue): ?>
                <div class="col s2 albums-catlog">
                    <a class="tkw-body-content" id="<?php echo $subValue['Link']; ?>" href="javascript:void(0)">
                        <span class="tkw-url-redirect" id="<?php echo $subValue['URLRedirect']; ?>"></span>
                        <div class="card small albums-img" >
                            <div class="card-image album-image">
                                <img src="<?php echo $subValue['ThumbnailImageWeb']; ?>" onerror="imgError(this);" title="<?php echo $subValue['Name']; ?>">
                                <span class="card-title" title="<?php echo $subValue['Name']; ?>"> <i class="fa fa-music album-play" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="card-content">
                            <span class="ellipsis" id="singer"><?php echo $subValue['Name']; ?></span>
                            <span class="album-by"><span class="album-artist ellipsis"> <?php
                                    if (isset($subValue['Artist'])) {
                                        echo $subValue['Artist'];
                                    }
                                    ?></span>
                            </span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <span class="view-all-title"><a class="tkw-body-content" href="javascript:void(0)" id="<?php echo 'index.php/web/category/' . $value[0]['CPId']; ?>" ><i class="material-icons right icon-spacing"><span class="tkw-url-redirect" id="<?php echo 'index.php/main/category/' . $value[0]['CPId']; ?>"></span>arrow_forward</i>Explore <?php echo $key; ?> </a></span>
        </div>
    <?php endforeach; ?>
</div>
