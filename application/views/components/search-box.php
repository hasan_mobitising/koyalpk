<span id="search-btn"><i id="srch"  class="material-icons">search</i></span>
<div class="input-field searchbox" id="search2">
    <input type="search" placeholder="Search Album..." id="search" maxlength="25" class="tkw-search-box" >
    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
    <i class="material-icons search-box-clear">close</i>
</div>
<div class="input-field searchbox" id="srchbox">
    <input type="search" placeholder="Search Album..." id="search" maxlength="25" class="tkw-search-box">
    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
    <i class="material-icons search-box-clear">close</i>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-script.js') ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/vendor/EasyAutocomplete-1.3.5/easy-autocomplete.min.css') ?>"> 
<link rel="stylesheet" href="<?php echo base_url('assets/vendor/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css') ?>"> 
<script src="<?php echo base_url('assets/vendor/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js') ?>"></script> 


<script>
    $(document).ready(function () {
        var options = {

            url: function (phrase) {
                return base_url + "index.php/web/suggestion/" + phrase;
            },

            getValue: function (element) {
                return element.name;
            },

            ajaxSettings: {
                dataType: "json",
                method: "POST",
                data: {
                    dataType: "json"
                }
            },

            preparePostData: function (data) {
                data.phrase = $(".tkw-search-box").val();
                return data;
            },

            requestDelay: 400
        };


        $(".tkw-search-box").easyAutocomplete(options);
        
        $(".tkw-search-box").keyup(function (event) {

            if (event.keyCode === 13) {
                event.preventDefault();
                var keyword = $(this).val();
                $.ajax({
                    async: true,
                    url: base_url + "index.php/web/search/" + keyword,
                    method: "POST",
                    data: {txtKeyword: keyword},
                    success: function (response) {
                        $(".tkw-body").html(response);
                        $(".tkw-search-box").val(keyword);
                        $('.button-collapse').sideNav('destroy');
                        $(".button-collapse").sideNav();
                    }
                });
            }
        });

    });
</script>