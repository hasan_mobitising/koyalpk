<!--Import Google Icon Font-->
<?php echo link_tag('https://fonts.googleapis.com/icon?family=Material+Icons') ?>


<!--Import materialize.css-->
<?php echo link_tag('assets/materialize/css/materialize.min.css') ?>
<?php echo link_tag('assets/font-awesome/css/font-awesome.min.css') ?>


<?php echo link_tag('assets/materialize/css/Style.css') ?>
<?php echo link_tag('assets/materialize/css/carousel.css') ?>
<?php echo link_tag('assets/materialize/css/Style_by_id.css') ?>
<?php echo link_tag('assets/materialize/css/media.css') ?>

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<title>Koyal - Sur’on Ki Rani</title>
<meta name="description" content="Koyal Music App par muft online Stream karain Urdu aur beshumar ilaqai zabanon mai apny pasandida Pakistani songs, Nusrat Fateh Ali Khan, Abida Parveen, Attaullah Esakhelvi, Rahat Fateh Ali Khan se ley ke Gul Panra, Anmol Siyal, Akram Rahi aur boht se mashoor singers’ kay songs kisi bhi waqt bilkul wo be bina kisi charges yani bilkul muft." />
<meta name="keywords" content="Nusrat Fateh Ali Khan, Abida Parveen, Attaullah Esakhelvi, Rahat Fateh Ali Khan, Gul Panra, Anmol Siyal, Akram Rahi, mashoor singers." />

<script>
    var base_url = "<?php echo base_url(); ?>";
</script>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="<?php echo base_url('assets/materialize/js/jquery-3.2.1.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/materialize/js/materialize.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/materialize/js/Custom.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-script.js') ?>"></script>
<!--<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-google-analytics.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-adwords.js') ?>"></script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script> -->
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/930430953/?guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
    function imgError(image) {
        var base_url = "<?php echo base_url(); ?>";
        image.onerror = "";
        image.src = base_url + "images/error_image.jpg";
        return!0
    }
</script>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-fb-pixel.js') ?>"></script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1059954747382062&ev=PageView&noscript=1" />
</noscript>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-page-view.js') ?>"></script>
