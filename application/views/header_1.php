<!--Import Google Icon Font-->
<?php echo link_tag('https://fonts.googleapis.com/icon?family=Material+Icons') ?>

<?php echo link_tag('assets/materialize/css/Style.css') ?>
<?php echo link_tag('assets/materialize/css/media.css') ?>


<!--Import materialize.css-->
<?php echo link_tag('assets/materialize/css/materialize.min.css') ?>
<?php echo link_tag('assets/font-awesome/css/font-awesome.min.css') ?>

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<title>Koyal - Sur’on Ki Rani</title>
<meta name="description" content="Koyal Music App par muft online Stream karain Urdu aur beshumar ilaqai zabanon mai apny pasandida Pakistani songs, Nusrat Fateh Ali Khan, Abida Parveen, Attaullah Esakhelvi, Rahat Fateh Ali Khan se ley ke Gul Panra, Anmol Siyal, Akram Rahi aur boht se mashoor singers’ kay songs kisi bhi waqt bilkul wo be bina kisi charges yani bilkul muft." />
<meta name="keywords" content="Nusrat Fateh Ali Khan, Abida Parveen, Attaullah Esakhelvi, Rahat Fateh Ali Khan, Gul Panra, Anmol Siyal, Akram Rahi, mashoor singers." />

<script>
var base_url = "<?php echo base_url(); ?>";
</script>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="<?php echo base_url('assets/materialize/js/jquery-3.2.1.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/materialize/js/materialize.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-script.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/tkw/js/tkw-jw-player.js') ?>"></script>

<script>function imgError(image){var base_url="<?php echo base_url(); ?>";image.onerror="";image.src=base_url+"images/error_image.jpg";return!0}</script><!-- Google Code for Remarketing Tag --><!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories.See more information and instructions on how to setup the tag on:http:---------------------------------------------------><script type="text/javascript">var google_tag_params={dynx_itemid:'REPLACE_WITH_VALUE',dynx_itemid2:'REPLACE_WITH_VALUE',dynx_pagetype:'REPLACE_WITH_VALUE',dynx_totalvalue:'REPLACE_WITH_VALUE',};</script><script type="text/javascript">var google_conversion_id=930430953;var google_custom_params=window.google_tag_params;var google_remarketing_only=!0;</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/930430953/?guid=ON&amp;script=0"/></div></noscript><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s)
{if(f.fbq)
return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)
f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init','1059954747382062');fbq('track','PageView');</script><noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1059954747382062&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code --><!--Google Analytics Script--><script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-110852046-1','auto');ga('send','pageview');</script><!-- Facebook Pixel Code --><script>!function(f,b,e,v,n,t,s)
{if(f.fbq)
return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)
f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init','173137780093025');fbq('track','PageView');</script><noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=173137780093025&ev=PageView&noscript=1"/></noscript><!-- End Facebook Pixel Code --><!-- Google Code for Koyal PageView Conversion Page --><script type="text/javascript">/ <![CDATA[ /
var google_conversion_id=824613887;var google_conversion_label="qZ3kCJHsrHoQ_7eaiQM";var google_remarketing_only=!1;/]]>/</script><script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/824613887/?label=qZ3kCJHsrHoQ_7eaiQM&amp;guid=ON&amp;script=0"/></div></noscript>
