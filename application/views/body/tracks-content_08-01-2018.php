<div class="row mble_nav">
    <?php $this->load->view('components/main-nav'); //tkw_array_debug($Tracks); ?>
</div>

<div class="row center-albums">
    <div class="col s12 mble-view">
        <div class="col s11 album-bg-img">
            <div class=" blur-img" style="background:black url(<?php echo $Tracks[0]['ThumbnailImage']; ?>)">
            </div>
            <div class="col s3 album-main-img">
                <a class="tkw-album" id="<?php echo $Tracks[0]['AlbumId']; ?>" href="javascript:void(0)">
                    <div class="card profile-img">
                        <div class="card-image album-main-image">
                            <img class="tkw-thumbnail-image" src="<?php echo $Tracks[0]['ThumbnailImage']; ?>">
                        </div>
                    </div>
                </a>
            </div>
            <h3 class="album-title"><?php echo $Tracks[0]['Album']; ?></h3>
            <h6 class="album-subtitle" style="margin-left: 30%;   margin-bottom: 10px;"></h6>
            <h6 class="album-subtitle"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <?php echo $Tracks[0]['NoOfLikes']; ?><i class="fa fa-share-alt" aria-hidden="true"></i><?php echo $Tracks[0]['NoOfShares']; ?></h6>
        </div>
        <div class="col s11 album-tracks-data">
            <div class="col s9 offset-s3 tracks">
                <div class="col s12 tracks-list">
                    <?php $this->load->view('components/append-tracks'); ?>
                </div>
                <?php if (count($Tracks) >= 50): ?>
                    <span class="tkw-show-more-tracks view-all-title" id="<?php echo isset($Action) ? $Action : ''; ?>" view-all-title"><a href="javascript:void(0)"><i class="material-icons left icon-spacing">add_circle</i> See More</a></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var track_url = window.location.href;
        var trackId = new URL(track_url).searchParams.get("trackId");
        currentTrackPlaying = trackId;
        if (trackId) {
            var filePath = $("#track-" + trackId).parent().parent().attr("id");
            tkw_audio_play(filePath);
            $("#track-" + trackId).addClass("fa-pause");
        } else {
            if (jwplayer('s3bubble-jwplayer').getState() == null) {
                var filePath = $(".fa-play").parent().parent().attr("id");
                tkw_audio_play(filePath);
                $("#" + $(".fa-play").attr("id")).addClass("fa-pause");
                trackId=$(".fa-play").attr("id");
                currentTrackPlaying = trackId;
            }

        }
        jwplayer().on('pause', function (e) {
            $("#" + trackId).removeClass("fa-pause");
            var endTime = jwplayer().getPosition();
            var duration = jwplayer().getDuration();
            tkw_track_stopped(currentTrackPlaying, duration, endTime);
        });

        jwplayer().on('play', function (e) {
            $("#" + trackId).addClass("fa-pause");
            tkw_track_played(currentTrackPlaying);
        });
        

        $(".page-footer").css("display", "none");
    });
</script>
