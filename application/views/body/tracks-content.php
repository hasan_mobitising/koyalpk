<div class="row mble_nav">
    <?php $this->load->view('components/main-nav'); //tkw_array_debug($Tracks); ?>
</div>

<div class="row center-albums">
    <div class="col s12 mble-view">
        <div class="col s11 album-bg-img">
            <div class=" blur-img tkw-blur-img" style="background:black url(<?php echo $Tracks[0]['ThumbnailImage']; ?>)" onerror="imgError(this);">
            </div>
            <div class="col s3 album-main-img padding-zero">
                <a class="tkw-album" id="<?php echo $Tracks[0]['AlbumId']; ?>" href="javascript:void(0)">
                    <div class="card profile-img">
                        <div class="card-image album-main-image">
                            <img class="tkw-thumbnail-image" src="<?php echo $Tracks[0]['ThumbnailImage']; ?>" onerror="imgError(this);">
                        </div>
                    </div>
                </a>
            </div>
            <h3 class="category-title">Album</h3>
            <h3 class="album-title"><?php echo $Tracks[0]['Album']; ?></h3>
            <h3 class="artist-title"><?php echo $Tracks[0]['Artist']; ?></h3>
            <h6 class="album-subtitle" style="margin-left: 30%;   margin-bottom: 10px;"></h6>
            <h6 class="album-subtitle"><span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span> <span><i class="fa fa-share" id="myBtn" aria-hidden="true"></i></span></h6>
            <h6 class="album-subtitle"><span class="album-count"><?php echo $Tracks[0]['NoOfLikes']; ?> Likes </span> <span class="album-count"><?php echo $Tracks[0]['NoOfShares']; ?> Shares</span></h6>
        </div>
        <div class="col s11 album-tracks-data">
            <div class="col s9 offset-s3 tracks">
                <div class="col s12 tracks-list">
                    <?php $this->load->view('components/append-tracks'); ?>
                </div>
                <?php if (count($Tracks) >= 50): ?>
                    <span class="tkw-show-more-tracks view-all-title" id="<?php echo isset($Action) ? $Action : ''; ?>" view-all-title><a href="javascript:void(0)"><i class="material-icons left icon-spacing">add_circle</i> See More</a></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--<div class="cplayer-img">
    <img class="tkw-thumbnail-image" src="<?php echo $Tracks[0]['ThumbnailImage']; ?>">
</div>-->

<script type="text/javascript">

    $(document).ready(function () {

        var new_global_playlist = JSON.parse('<?php echo global_playlist($Tracks) ?>');
        globalPlaylist = remove_duplicates(globalPlaylist.concat(new_global_playlist));

        var playlist = '<?php echo get_playlist($Tracks) ?>';
        playlist = JSON.parse(playlist);
        var state = jwplayer('s3bubble-jwplayer').getState();

        if (state !== "playing") {
            jwplayer('s3bubble-jwplayer').setup({"playlist": playlist, "repeat": true, "bufferlength": 10});
        } else {
            for (let i = 0; i < playlist.length; i++) {
                var ext = playlist[i]['file'].substr((playlist[i]['file'].lastIndexOf('.') + 1));
                jwplayer('s3bubble-jwplayer').getPlaylist().push({
                    "sources": [{"default": true, "file": playlist[i]['file'], "label": playlist[i]['title'], "type": ext}],
                    "description": "",
                    "minDvrWindow": 120,
                    "title": playlist[i]['title'],
                    "image": playlist[i]['image'],
                    "file": playlist[i]['file'],
                    "preload": "none",
                    "tracks": [{"file": playlist[i]['file'], "label": playlist[i]['title'], "kind": "thumbnails"}]
                });
            }
            playlist = jwplayer('s3bubble-jwplayer').getPlaylist();
        }


        var trackUrl = window.location.href;
        var getTrackId = new URL(trackUrl).searchParams.get("trackId");

        if (getTrackId) {
            for (let i = 0; i < globalPlaylist.length; i++) {
                if (globalPlaylist[i].TrackId === getTrackId) {
                    playlist = jwplayer('s3bubble-jwplayer').getPlaylist();
                    for (let index = 0; index < playlist.length; index++) {
                        if (playlist[index].title == globalPlaylist[i].Track) {
                            jwplayer('s3bubble-jwplayer').playlistItem(index);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        jwplayer('s3bubble-jwplayer').onReady(function (event) {
            jwplayer('s3bubble-jwplayer').setMute(false);
        });

        jwplayer('s3bubble-jwplayer').on('all', function () {
            $(".jw-icon-next").hide();
        });

        jwplayer('s3bubble-jwplayer').on('buffer', function () {
            let index = jwplayer('s3bubble-jwplayer').getPlaylistIndex();
            let item = jwplayer('s3bubble-jwplayer').getPlaylistItem(index);
            let title = item['title'];
            let current = $('.tracks-name:contains(' + title + ')');
            current.prev().addClass("fa-pause");
        });

        jwplayer('s3bubble-jwplayer').on('play', function (event) {
            $('.fa-play').removeClass('fa-pause');
            let index = jwplayer('s3bubble-jwplayer').getPlaylistIndex();
            let item = jwplayer('s3bubble-jwplayer').getPlaylistItem(index);
            $(".tkw-thumbnail-image").attr("src",item['image']);
            let title = item['title'];
            let thumbnailImage = $('.tkw-album-image[data-trackTitle="'+title+'"]').attr('data-trackImage');
            $('.tkw-blur-img').css('background-image', 'url(' + thumbnailImage + ')');
            $('.tkw-thumbnail-image').attr('src',thumbnailImage);
            $('.fa-play[data-tracktitle="'+title+'"]').addClass('fa-pause');
            for (let i = 0; i < globalPlaylist.length; i++) {
                if (globalPlaylist[i].Track == title) {
                    tkw_track_played(globalPlaylist[i].TrackId, globalPlaylist[i].Genre, globalPlaylist[i].Language, globalPlaylist[i].Track, globalPlaylist[i].Artist, globalPlaylist[i].Album, globalPlaylist[i].ArtistId, globalPlaylist[i].AlbumId);
                    break;
                }
            }
        });

        jwplayer('s3bubble-jwplayer').on('pause', function (event) {
            $('.fa-play').removeClass('fa-pause');
            let index = jwplayer('s3bubble-jwplayer').getPlaylistIndex();
            let item = jwplayer('s3bubble-jwplayer').getPlaylistItem(index);
            let title = item['title'];
            let endTime = jwplayer('s3bubble-jwplayer').getPosition();
            let duration = jwplayer('s3bubble-jwplayer').getDuration();
            for (let i = 0; i < globalPlaylist.length; i++) {
                if (globalPlaylist[i].Track == title) {
                    tkw_track_stopped(globalPlaylist[i].TrackId, duration, endTime, globalPlaylist[i].Genre, globalPlaylist[i].Language, globalPlaylist[i].Track, globalPlaylist[i].Artist, globalPlaylist[i].Album, globalPlaylist[i].ArtistId, globalPlaylist[i].AlbumId);
                    break;
                }
            }
        });



        jwplayer('s3bubble-jwplayer').on('complete', function () {
            last_track_hit(globalPlaylist);
        });



        $(".tracksmallimg").on("click", function () {
            last_track_hit(globalPlaylist);
            $(".fa-play").removeClass('fa-pause');
            let title = $(this).attr('data-trackTitle');
            playlist = jwplayer('s3bubble-jwplayer').getPlaylist();
            for (let index = 0; index < playlist.length; index++) {
                if (playlist[index].title == title.trim()) {
                    if (index !== jwplayer('s3bubble-jwplayer').getPlaylistIndex()) {
                        jwplayer('s3bubble-jwplayer').playlistItem(index);
                    } else {
                        jwplayer('s3bubble-jwplayer').play();
                    }
                    break;
                }
            }
        });


        $(".page-footer").css("display", "none");
    });
</script>
