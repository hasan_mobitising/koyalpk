<?php echo $this->load->view("components/navbar"); ?>
<div class="row center-albums" id="Collections">

    <span class="result-for"><?php echo isset($_GET['keyword']) ? "Showing Result for " . $_GET['keyword'] : ''; ?></span>

    <?php foreach ($SearchResult as $key => $value): ?>

        <div class="col s12" id="<?php echo $key; ?>">
            <div id="titles">
                <h5 class="k-title home_title"><span><?php echo $key; ?></span></h5>
                <h5 class="k-title home_urdutitle"><span><?php echo $value[0]['CatInUrdu']; ?></span></h5>
            </div>
            <?php
            if ($key == "Singers" || $key == "Albums" || $key == "Collection") {
                foreach ($value as $subValue):
                    ?>
                    <div class="col s2 albums-catlog">
                        <a class="tkw-body-content" id="<?php echo $subValue['Link']; ?>" href="javascript:void(0)">
                            <span class="tkw-url-redirect" id="<?php echo $subValue['URLRedirect']; ?>"></span>
                            <div class="card small albums-img" >
                                <div class="card-image album-image">
                                    <img src="<?php echo $subValue['ThumbnailImageWeb']; ?>" onerror="imgError(this);" title="<?php echo $subValue['Name']; ?>">
                                    <span class="card-title" title="<?php echo $subValue['Name']; ?>"> <i class="fa fa-music album-play" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div class="card-content">
                                <span class="ellipsis" id="singer"><?php echo $subValue['Name']; ?></span>
                                <span class="album-by">
                                    <span class="album-artist ellipsis"> 
                                        <?php echo (isset($subValue['Artist'])) ? $subValue['Artist'] : "Unknown Artist"; ?>
                                    </span>
                                </span>
                            </div>
                        </a>
                    </div>
                    <?php
                endforeach;
            } elseif ($key == "Songs") {
                ?> 
                <div class="col s11 album-tracks-data">
                    <div class="col s9 offset-s3 tracks">
                        <?php foreach ($value as $subValue): ?>
                            <div class="col s12 tracks-list">
                                <div class="col s12 track">
                                    <div id="<?php echo $subValue['TrackUrl']; ?>" class="tracklist-margin">
                                        <div class="col s2 albums-catlog Track-catlog">
                                            <div class="card small albums-img tracks-img">
                                                <div class="card-image album-image tracksmallimg">
                                                    <img src="<?php echo $subValue['ThumbnailImage']; ?>" onerror="imgError(this);">
                                                    <span class="card-title"> <i class="fa fa-play album-play" id="<?php echo "track-" . $subValue['TrackId']; ?>" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s7 track-detail">
                                            <a class="track-name" href="<?php echo $subValue['URLRedirect']; ?>">
                                                <span class="tracks-name ellipsis"><?php echo " " . $subValue['Name']; ?></span></a>
                                            <a class="by-artist ellipsis" href="<?php echo base_url() . 'main/artist/' . $subValue['ArtistId'] . '/' . str_replace(' ', '-', $subValue['Artist']) ?>"><?php echo $subValue['Artist']; ?></a>
                                        </div>
                                        <div class="col s3 padding-zero">
                                            <div class="col s6 padding-zero">
                                                <i class="fa fa-thumbs-up padding-zero option-icon" id="<?php echo "track-" . $subValue['TrackId']; ?>" aria-hidden="true"></i>
                                                <span class="options-txt">Like</span>
                                            </div>
                                            <div class="col s6 padding-zero">
                                                <i class="fa fa-share padding-zero option-icon Modal1" id="<?php echo "track-" . $subValue['TrackId']; ?> " aria-hidden="true"></i>
                                                <span class="options-txt">Modal</a></span>									
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php } ?>
            <span class="view-all-title"><a class="tkw-body-content" href="javascript:void(0)" id="<?php echo 'index.php/web/category/' . $value[0]['CPId']; ?>" ><i class="material-icons right icon-spacing"><span class="tkw-url-redirect" id="<?php echo 'index.php/main/category/' . $value[0]['CPId']; ?>"></span>arrow_forward</i>Explore <?php echo $key; ?> </a></span>
        </div>
    <?php endforeach; ?>
    <script>
        $(document).ready(function () {
            var limit = 5;
            var offset = 0;
            $(".view-all-title").click(function () {
                offset = offset + 5;
                var type = $(this).attr("id");
                var keyword = $("#search").val();
                $.ajax({
                    async: true,
                    url: base_url + "index.php/web/search_more/",
                    method: "POST",
                    data: {type: type, keyword: keyword, limit: limit, offset: offset},
                    success: function (response) {
                        $("." + type).append(response);
                    }
                });

            });
        });
    </script>