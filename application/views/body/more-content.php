<?php echo $this->load->view("components/navbar"); ?>
<div class="row center-albums tkw-albums">
    <?php $this->load->view('components/albums'); ?>
</div>
<script>
    $(document).ready(function () {

        var limit = 12;
        var offset = 24;

        $(".tkw-show-more").click(function () {
            var action = $(".tkw-action").attr("id");
            var data = {action: action, limit: limit, offset: offset};
            $.ajax({
                async: true,
                url: base_url + "index.php/web/more_albums",
                method: "POST",
                data: data,
                success: function (response) {
                    offset = offset + limit;
                    if (response == "") {
                        $(".tkw-count").hide();
                    } else {
                        $(".tkw-sorted-content").append(response);

                    }
                }
            });
        })

    });

</script>

