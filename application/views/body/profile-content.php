<div class="row mble_nav">
    <?php $this->load->view('components/main-nav'); // tkw_array_debug($Tracks); ?>
</div>
<div class="row center-albums singer-profile">
    <div class="col s12 mble-view" id="Artists">
        <div id="titles" class="ptitle">
            <h5 class="k-title home_title"><span>Artist</span></h5>
            <h5 class="k-title home_urdutitle"><span>[Title urdu]</span></h5>
        </div>
        <div class="col s11 profile-container">
            <h3 class="a-title"><?php echo $Artist ?></h3>
            <h6 class="a-subtitle"><span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></span> <span><i class="fa fa-share" aria-hidden="true"></i></span></h6>
            <h6 class="a-subtitle"><span class=""><?php echo $Likes ?> Likes</span> <span class=""><?php echo $Shares; ?> Shares</span></h6>
            <div class="col s4">
                <h6 class="a-subtitle">
                    <span class="detail-title">Style</span>
                    <span><?php echo $Style ?></span>
                </h6>
            </div>
            <div class="col s4">
                <h6 class="a-subtitle">
                    <span class="detail-title">Albums</span>
                    <span><?php echo $NoOfAlbums ?></span>
                </h6>
            </div>
            <div class="col s4">
                <h6 class="a-subtitle">
                    <span class="detail-title">Songs</span>
                    <span><?php echo $NoOfTracks ?></span>
                </h6>
            </div>  
        </div>
        <div class="col s12">
            <div id="titles">
                <h5 class="k-title home_title"><span>Albums</span></h5>
                <h5 class="k-title home_urdutitle"><span>[Title urdu]</span></h5>
            </div>
            <?php foreach ($Albums as $key): ?>
                <div class="col s2 albums-catlog">
                    <a class="tkw-body-content" id="<?php echo $key['Link']; ?>" href="javascript:void(0)">
                        <span class="tkw-url-redirect" id="<?php echo $key['URLRedirect']; ?>"></span>
                        <div class="card small albums-img" >
                            <div class="card-image album-image">
                                <img src="<?php echo $key['ThumbnailImageWeb']; ?>" onerror="imgError(this);" title="<?php echo $key['Name']; ?>">
                                <span class="card-title" title="<?php echo $key['Name']; ?>"> <i class="fa fa-music album-play" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="card-content">
                            <span class="ellipsis" id="singer"><?php echo $key['Name']; ?></span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <span class="view-all-title"><a class="tkw-body-content" href="javascript:void(0)" id="<?php echo $AllAlbumsLinkWeb ?>"><i class="material-icons right icon-spacing"><span class="tkw-url-redirect" id="<?php echo $AllAlbumsLinkMain ?>"></span>arrow_forward</i>Explore <?php echo $key; ?> </a></span>
        </div>
        <div class="col s12">
            <div id="titles">
                <h5 class="k-title home_title"><span>Tracks</span></h5>
                <h5 class="k-title home_urdutitle"><span>[Title urdu]</span></h5>
            </div>
            <div class="col s11 album-tracks-data">
                <div class="col s9 offset-s3 tracks">
                    <div class="col s12 tracks-list">
                        <?php foreach ($Tracks as $key) : ?>
                            <div class="col s12 track">
                                <div class="tracklist-margin">
                                    <div class="col s2 albums-catlog Track-catlog">
                                        <div class="card small albums-img tracks-img">
                                            <div class="card-image album-image tracksmallimg" data-trackTitle = "<?php echo $key['Name']; ?>">
                                                <img src="<?php echo $key['ThumbnailImageWeb']; ?>" onerror="imgError(this);">
                                                <span class="card-title"> <i class="fa fa-play album-play" id="<?php echo "track-" . $key['TrackId']; ?>" data-trackTitle = "<?php echo $key['Name']; ?>" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s7 track-detail">
                                        <a class="track-name" href="<?php echo $key['URLRedirect'] ?>">
                                            <span class="tracks-name ellipsis tracksmallimg" data-trackTitle = "<?php echo $key['Name']; ?>"><?php echo " " . $key['Name']; ?></span>
                                        </a>
                                    </div>
                                    <div class="col s3 padding-zero">
                                        <div class="col s6 padding-zero">
                                            <i class="fa fa-thumbs-up padding-zero option-icon" id="<?php echo "track-" . $key['TrackId']; ?>" aria-hidden="true"></i>
                                            <span class="options-txt">Like</span>
                                        </div>
                                        <div class="col s6 padding-zero">
                                            <i class="fa fa-share padding-zero option-icon Modal tkw-copy-share-url-track" id="<?php echo "track-" . $key['TrackId']; ?> " data-trackId="<?php echo $key['TrackId']; ?> " aria-hidden="true"></i>
                                            <span class="options-txt">Modal</a></span>									
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
