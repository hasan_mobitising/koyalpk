<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('header'); ?>
    </head>

    <body>
        <div class="row">
            <div class="profile-scroll">
                <a href="Profile.html"><i class="fa fa-heart fa_profile" aria-hidden="true"></i></a>
            </div>
            <?php $this->load->view('main-nav'); ?>
        </div>

        <div class="row center-albums tkw-albums">
            <?php $this->load->view('components/albums'); ?>
        </div>

        <footer class="page-footer">
            <?php $this->load->view('footer'); ?>
        </footer>
        <script>
            $(document).ready(function () {

                var limit = 12;
                var offset = 24;

                $(".tkw-show-more").click(function () {
                    var action = $(".tkw-action").attr("id");
                    var url = "<?php echo base_url() . 'index.php/main/more_albums'; ?>"
                    var data = {action: action, limit: limit, offset: offset};
                    $.ajax({
                        async: true,
                        url: url,
                        method: "POST",
                        data: data,
                        success: function (response) {
                            console.log(response);
                            offset = offset + limit;
                            if (response == "") {
                                $(".tkw-count").hide();
                            } else {
                                $(".tkw-sorted-content").append(response);
                                
                            }
                        }
                    });
                });


                $(document).on("click", ".tkw-nav-link", function () {
                    var languageId = $(this).attr("id");
                    var url = window.location.pathname;
                    var categoryId = url.split("/").slice(-1)[0];
                    var finalUrl = "<?php echo base_url() . 'index.php/main/category_ajax/'; ?>" + categoryId;
                    $.ajax({
                        async: true,
                        url: finalUrl,
                        method: "POST",
                        data: {languageId: languageId},
                        success: function (response) {
                            $(".tkw-albums").html(response);
                        }
                    });
                });

            });

        </script>
    </body>
</html>
