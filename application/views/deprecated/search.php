<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('header'); ?>
    </head>

    <body>
        <div class="row">
            <div class="profile-scroll">
                <a href="javascript:void(0)"><i class="fa fa-heart fa_profile" aria-hidden="true"></i></a>
            </div>
            <?php $this->load->view('main-nav-temp'); ?>
        </div>

        <div class="row center-albums">

            <span class="result-for"><?php echo isset($_GET['keyword']) ? "Showing Result for " . $_GET['keyword'] : ''; ?></span>

            <?php
            foreach ($SearchResult as $key => $value):
                ?>
                <div class="col s12 margin-bottom-30">
                    <div style="display:inline-block; width:100%; margin-bottom:10px; ">
                        <div class="col s2 k-title">
                            <div class="col s11 viewmore-title"><h5><?php echo $key ?></h5></div>
                        </div>
                    </div>
                    <?php foreach ($value as $subValue): ?>
                        <div class="col m9 search-result">
                            <div class="col s2 albums-catlog catlog-space-top">
                                <a href="<?php echo $subValue['Link'] ?>">
                                    <div class="card small albums-img">
                                        <div class="card-image album-image">
                                            <img src="<?php echo $subValue['ThumbnailImage'] ?>" onerror="imgError(this);">
                                            <span class="card-title"> <i class="fa fa-play album-play" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col m10 mble-search">
                                <div class="col m7 search-title">
                                    <span class="artist"><a href="<?php echo $subValue['Link'] ?>"><?php echo $subValue['Name'] ?></a></span>
                                    <?php if (isset($subValue['Album'])) : ?>
                                        <span class="album"><?php echo $subValue['Album'] ?></span>
                                    <?php endif; ?>
                                    <?php if (isset($subValue['Artist'])) : ?>
                                        <span class="author"><?php echo $subValue['Artist'] ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="<?php echo $key ?>"></div>
                    <?php if (count($value) > 4) : ?>
                        <div class="col m9 margin-20">
                            <span class="view-all-title" id="<?php echo $key ?>"><a href="javascript:void(0)"><i class="material-icons left icon-spacing">add_circle</i> See More</a></span>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>

        <footer class="page-footer">
            <?php $this->load->view('footer'); ?>
        </footer>

        <script>

            $(document).ready(function () {

                var base_url = "<?php echo base_url(); ?>";
                var limit = 4;
                var offset = 0;

                $(".view-all-title").click(function () {
                    offset = offset + 4;
                    var type = $(this).attr("id");
                    var keyword = $("#search").val();
                    $.ajax({
                        async: true,
                        url: base_url + "index.php/main/search_more/",
                        method: "POST",
                        data: {type: type, keyword: keyword, limit: limit, offset: offset},
                        success: function (response) {
                            $("." + type).append(response);
                        }
                    });

                });
            });
        </script>
    </body>
</html>
