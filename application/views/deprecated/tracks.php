<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('header'); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Cache-control" content="public">
        <meta property="og:url"           content="<?php echo $_SERVER['REQUEST_URI']; ?>" />
        <meta property="og:type"          content="website" />
        <meta property="og:title" content="<?php echo $Tracks[0]['Album']; ?>">
        <meta property="og:description"   content="<?php echo $Tracks[0]['Album']; ?>" />
        <meta property="og:image"         content="<?php echo $Tracks[0]['ThumbnailImage']; ?>" />
        <title> <?php echo $Tracks[0]['Album']; ?> | Koyeel </title>
    </head>

    <body>
        <div class="row mble_nav">
            <?php $this->load->view('main-nav-temp'); //tkw_array_debug($Tracks); ?>
        </div>

        <div class="row center-albums">
            <div class="col s12 mble-view">
                <div class="col s11 album-bg-img">
                    <div class=" blur-img" style="background:black url(<?php echo $Tracks[0]['ThumbnailImage']; ?>)">
                    </div>
                    <div class="col s3 album-main-img">
                        <a class="tkw-album" id="<?php echo $Tracks[0]['AlbumId']; ?>" href="javascript:void(0)">
                            <div class="card profile-img">
                                <div class="card-image album-main-image">
                                    <img class="tkw-thumbnail-image" src="<?php echo $Tracks[0]['ThumbnailImage']; ?>">
                                </div>
                            </div>
                        </a>
                    </div>
                    <h3 class="album-title"><?php echo $Tracks[0]['Album']; ?></h3>
                    <h6 class="album-subtitle" style="margin-left: 30%;   margin-bottom: 10px;"> By <?php echo $Tracks[0]['Artist']; ?></h6>
                    <h6 class="album-subtitle"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <?php echo $Tracks[0]['NoOfLikes']; ?><i class="fa fa-share-alt" aria-hidden="true"></i><?php echo $Tracks[0]['NoOfShares']; ?></h6>
                </div>
                <div class="col s11 album-tracks-data">
                    <div class="col s9 offset-s3 tracks">
                        <div class="col s12 tracks-list">
                            <?php $this->load->view('components/append-tracks'); ?>
                        </div>
                        <?php if (count($Tracks) >= 50): ?>
                            <span class="tkw-show-more-tracks view-all-title" id="<?php echo isset($Action) ? $Action : ''; ?>" view-all-title"><a href="javascript:void(0)"><i class="material-icons left icon-spacing">add_circle</i> See More</a></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="s3bubble-jwplayer"></div>
        <footer class="page-footer">
            <?php $this->load->view('footer'); ?>
        </footer>

        <script src="//content.jwplatform.com/libraries/DbXZPMBQ.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                var trackId = window.location.hash.substr(1);
                var base_url = "<?php echo base_url(); ?>";
                var limit = 4;
                var offset = 6;
                var albumId = $(".tkw-album").attr("id");

                if (trackId) {
                    var filePath = $("#" + trackId).parent().attr("id");
                    tkw_audio_play(filePath);
                    $("#" + trackId).addClass("fa-pause");
                }
                $(document).on("click", ".track-name", function () {
                    $(".track").css("border-bottom", "1px solid #d6d6d6");
                    $(this).parent().parent().parent().css("border-bottom", "2px solid #755eb6");
                    if ($(this).find(">:first-child").hasClass("fa-pause")) {
                        $(this).find(">:first-child").removeClass("fa-pause");
                        jwplayer('s3bubble-jwplayer').pause(true);
                        /* $("#s3bubble-jwplayer").hide(); */


                    } else {
                        $(".track-name").find(">:first-child").removeClass("fa-pause");
                        $(this).find(">:first-child").addClass("fa-pause");
                        var filePath = $(this).parent().attr("id");
                        /*if ($("#s3bubble-jwplayer").show()) {
                         $("#s3bubble-jwplayer").hide();
                         }*/
                        tkw_audio_play(filePath);
                        $(".page-footer").css("display", "none");
                    }


                });

                $(".tkw-show-more-tracks").click(function () {
                    var action = $(this).attr("id");
                    $.ajax({
                        async: true,
                        url: base_url + "index.php/main/more_tracks/",
                        method: "POST",
                        data: {albumId: albumId, action: action, limit: limit, offset: offset},
                        success: function (response) {
                            console.log(response);
                            offset = offset + 4;
                            if (response == "") {
                                $(".tkw-show-more-tracks").hide();
                            }
                            $(".tracks-list").append(response);

                        }
                    });

                });

            });

            function tkw_audio_play(filePath) {
                jwplayer('s3bubble-jwplayer').setup({
                    "playlist": {
                        "image": $(".tkw-thumbnail-image").attr("src"),
                        "sources": {
                            "file": filePath,
                            "preload": "none",
                        },
                    },
                    "autostart": true,
                    "width": "100%",
                    "aspectratio": "16:9",
                    "primary": "html5",
                    "skin": {
                        "name": "bekle",
                        "active": "",
                        "inactive": "",
                        "background": ""
                    },
                });

                jwplayer().onReady(function (event) {
                    jwplayer().setMute(false);
                });
            }


            /* function tkw_audio_play(filePath) {
             jwplayer('s3bubble-jwplayer').setup({
             "playlist": {
             "image": $(".tkw-thumbnail-image").attr("src"),
             "sources": {
             "file": filePath,
             "type": "hls",
             "preload": "none",
             },
             },
             "autostart": true,
             "width": "100%",
             "aspectratio": "16:9",
             "primary": "html5",
             "hlshtml": true,
             "skin": {
             "name": "bekle",
             "active": "",
             "inactive": "",
             "background": ""
             },
             "id": "s3bubble-jwplayer"
             });
             }*/
        </script>
        <style>
            .jwplayer.jw-flag-aspect-mode
            {
                height:65px !important;
            }
            .jw-icon-fullscreen
            {
                display:none !important;
            }
            .jw-flag-media-audio .jw-preview{
                background: #f7f7f7 !important;
            }
            video.jw-video.jw-reset
            {
                background: linear-gradient(90deg, #8563be, #304a95);
            }
            .jw-controlbar .jw-overlay
            {
                position:fixed !important;
                left: 92%!important;
                bottom: 63px !important
            }
            .jw-controlbar.jw-background-color.jw-reset
            {
                max-width: 100% !important;
                background: none;
                font-size: larger;
            }
            .jw-progress.jw-reset
            {
                background:white !important;
            }
            ul.jw-menu.jw-background-color.jw-reset{
                background: linear-gradient(90deg, #8563be, #304a95);
            }
            .jw-slider-volume.jw-volume-tip.jw-background-color.jw-reset.jw-slider-vertical.jw-reset
            {
                background: linear-gradient(90deg, #8563be, #304a95);
            }
            .jw-skin-bekle .jw-option.jw-active-option
            {
                color: #2f3847 !important;
                background-color: rgb(255, 255, 255) !important;
            }
            .jw-skin-bekle .jw-toggle
            {
                color: #ffffff !important;
            }

        </style>
    </body>
</html>
