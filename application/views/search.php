<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('header'); ?>
    </head>
    <body>

        <div class = "tkw-body">
            <?php $this->load->view('body/search-content'); ?>
        </div>

        <div id="s3bubble-jwplayer"></div>
        <script src="//content.jwplatform.com/libraries/DbXZPMBQ.js"></script>

        <footer class="page-footer">
            <?php $this->load->view('footer'); ?>
        </footer>

        <?php echo link_tag('assets/tkw/css/style.css') ?>
    </body>
</html>
