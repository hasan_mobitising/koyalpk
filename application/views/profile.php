<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('header'); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Cache-control" content="public">
        <meta property="og:url"           content="<?php echo 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>" />
        <meta property="og:type"          content="website" />
        <?php
        if (isset($Tracks)) {
            $metaTitle = $Tracks[0]['Album'] . " - " . $Tracks[0]['Artist'];
            $metaAlbum = $Tracks[0]['Album'] . " - " . $Tracks[0]['Artist'];
            foreach ($Tracks as $key) {
                if ($key['TrackId'] == $_GET['trackId']) {
                    $metaTitle = $key['Name'];
                    $metaAlbum = $Tracks[0]['Album'] . " - " . $Tracks[0]['Artist'];
                }
            }
        }
        ?>
        <meta id="tkw-meta-title" property="og:title" content="<?php echo $metaTitle; ?>">
        <meta property="og:description"   content="<?php echo $metaAlbum; ?>" />
        <meta property="og:image"         content="<?php echo $Tracks[0]['ThumbnailImageWeb']; ?>" />
        <title> <?php echo $Tracks[0]['Album']; ?> | Koyeel </title>
    </head>
    <body>
        <div class = "tkw-body">
            <?php $this->load->view('body/profile-content'); ?>
        </div>
        <div id="s3bubble-jwplayer"></div>
        <script src="//content.jwplatform.com/libraries/DbXZPMBQ.js"></script>

        <footer class="page-footer">
            <?php $this->load->view('footer'); ?>
        </footer>
        <?php echo link_tag('assets/tkw/css/style.css') ?>
    </body>
</html>
